var express = require('express');
var bodyParser = require('body-parser')
var app = express();

// Afficher le formulaire dans la console
var urlencodedParser = bodyParser.urlencoded ( { extended: false})

app.post('/', urlencodedParser, function (req, res){
    console.log(req.body);
});
// STOP

app.set ('view-engine', 'ejs') ;

app.use(express.static('public'));

app.get('/', function(req, res) {
    res.render('Les Capétiens.ejs');
});

app.get('/Hugues/', function(req, res) {
    res.render('Hugues Capet.ejs');
});

app.get('/auguste/', function(req, res) {
    res.render('Philippe Auguste.ejs');
});

app.get('/louisix/', function(req, res) {
    res.render('Louis IX.ejs');
});

app.get('/philippeiv/', function(req, res) {
    res.render('Philippe IV le Bel.ejs');
});

app.get('/contact/', function(req, res) {
    res.render('Contactez-nous.ejs');
});

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page introuvable !');
});

app.listen(8080);