'use strict';
var slideIndex = 1;
var mySto;

function plusSlides(n) {
  showSlides(slideIndex += n);
  clearTimeout(mySto);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  if (n==undefined){n = ++slideIndex}
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";

  mySto = setTimeout(showSlides, 5000);
}

